package com.example.demo.restcontroller;

import com.example.demo.entity.Book;
import com.example.demo.service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;

@RestController
public class BookRestController {

    @Autowired
    private BookService bookService;

    @GetMapping("/list")
    public List<Book> list() {
        List<Book> books = bookService.list();
        for (Book b : books) {
            System.out.println("Name: " + b.getName() + "\nId: " + b.getId() + "\n");
        }

        return books;
    }

    @GetMapping("/find")
    public Book findById(@RequestParam Long id) {
        Optional<Book> book = bookService.findById(id);

        if (book.isPresent()) {
            System.out.print("\nName: " + book.get().getName() + "\nId: " + book.get().getId() + "\n");
            return book.get();
        } else {
            System.out.print("No encontrado por ID '" + id + "'\n");
            return null;
        }
    }

}
